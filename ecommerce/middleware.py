from ecommerce import settings
from django.utils import translation
from product.models.Category import Category
from django.http import HttpResponseRedirect


class SetLanguage:


    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        slug = request.path.strip('/').split('/')[-1]
        language = request.GET.get('lang')
        for lang in settings.CHECK_LANGUAGES:
            if lang == language:
                category_page_filter = Category.objects.filter(translations__slug=slug)
                if len(category_page_filter):
                    category_page = category_page_filter[0]
                    category_page.set_current_language(language)
                    translation.activate(language)
                    request.session[translation.LANGUAGE_SESSION_KEY] = language
                    url = category_page.get_absolute_url()
                    return HttpResponseRedirect(url)
                translation.activate(language)
                request.session[translation.LANGUAGE_SESSION_KEY] = language

        response = self.get_response(request)
        return response
