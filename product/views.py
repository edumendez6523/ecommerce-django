from django.shortcuts import render
from .models.Category import Category
from .models.AttributeGroup import AttributeGroup
from .models.Brand import Brand
from django.utils import translation
from django.shortcuts import get_object_or_404, Http404, HttpResponse
from django.http import JsonResponse
import json
import ast
from helper.helper import query_debugger


def show_categories(request):
    return render(request, "categories.html", {'categories': Category.objects.all()})


def show_brands(request):
    return render(request, "brands.html", {'brands': Brand.objects.all()})


def show_single(request, full_slug):
    slugs = full_slug.split('/')
    category = None

    if len(slugs) > 1:
        category = get_object_or_404(Category, translations__slug=slugs[-1])
    elif len(slugs) == 1:
        category = get_object_or_404(Category, translations__slug=slugs[0])
    if category and category.get_absolute_url().strip('/') == full_slug:
        return render(request, 'category.html', {'category': category})
    else:
        raise Http404


def create_tree(categories, selected_list):
    list_data = []
    for category in categories:
        selected = "false"
        if selected_list is not None and category.id in selected_list:
            selected = "true"

        dict = {'id': category.id, 'text': category.title, 'selected': selected}
        if category.is_leaf_node:
            child_list_data = create_tree(category.get_children(), selected_list)
            dict['inc'] = child_list_data
        list_data.append(dict)
    return list_data


def get_categories(request):
    selected_categories_str = request.GET.get('category_values')
    selected_categories = ast.literal_eval(selected_categories_str)
    categories = Category.objects.filter(level=0)
    list = create_tree(categories, selected_categories)
    data = {'data': list}
    return JsonResponse(data)


@query_debugger
def get_attributes(request):
    data = json.loads(request.body)
    categories = data['categories']
    attribute_values_str = data['attribute_values']
    attribute_values = ast.literal_eval(attribute_values_str)

    categories = Category.objects.filter(id__in=categories).all().prefetch_related('attribute_group')
    list = []
    html = ''
    for category in categories:
        groups = category.attribute_group.all()
        for group in groups.prefetch_related('attrs'):
            if group not in list:
                list.append(group)

    for l in list:
        html += '<optgroup label="' + str(l.title) + '">'
        for attr in l.attrs.all():
            selected = ""
            if attribute_values is not None and attr.id in attribute_values:
                selected = "selected"

            html += '<option value="' + str(attr.id) + '" ' + str(selected) + '>' + str(attr.title) + '</option>'
        html += '</optgroup>'
    return HttpResponse(html)
