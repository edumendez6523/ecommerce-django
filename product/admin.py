from .models.Brand import Brand
from .modelAdmin.BrandAdmin import BrandAdmin
from .models.Category import Category
from .modelAdmin.CategoryAdmin import CategoryAdmin
from .models.Product import Product
from .modelAdmin.ProductAdmin import ProductAdmin
from .models.AttributeGroup import AttributeGroup
from .modelAdmin.AttributeGroupAdmin import AttributeGroupAdmin
from .models.Attribute import Attribute
from .modelAdmin.AttributeAdmin import AttributeAdmin
from django.contrib import admin

admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(AttributeGroup,AttributeGroupAdmin)
admin.site.register(Attribute,AttributeAdmin)
