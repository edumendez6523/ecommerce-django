from parler.admin import TranslatableAdmin
from adminsortable2.admin import SortableAdminMixin


class BrandAdmin(SortableAdminMixin,TranslatableAdmin):
    list_display = ('title', 'slug', 'image_tag')
    readonly_fields = ['image_tag']
    search_fields = ['translations__title']
