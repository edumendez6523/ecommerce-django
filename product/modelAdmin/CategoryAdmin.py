from mptt.admin import DraggableMPTTAdmin
from parler.admin import TranslatableAdmin, TranslatableModelForm
from mptt.admin import MPTTModelAdmin
from mptt.forms import MPTTAdminForm


class CategoryAdminForm(MPTTAdminForm, TranslatableModelForm):
    pass


class CategoryAdmin(DraggableMPTTAdmin, TranslatableAdmin):
    form = CategoryAdminForm
    mptt_indent_field = "title"
    list_display = ('tree_actions', 'indented_title', 'slug', 'image_tag')
    readonly_fields = ['image_tag']
    search_fields = ['translations__title']


