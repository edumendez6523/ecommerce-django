from parler.admin import TranslatableAdmin
from adminsortable2.admin import SortableAdminMixin
from ..models.Images import Images
from django.contrib import admin


class ImagesInline(admin.TabularInline):
    model = Images
    list_display = ('id', 'image', 'image_tag')
    readonly_fields = ['image_tag']


class ProductAdmin(SortableAdminMixin, TranslatableAdmin):
    list_display = ('title', 'slug')
    search_fields = ['translations__title']
    inlines = [
        ImagesInline
    ]
