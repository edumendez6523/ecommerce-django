from parler.admin import TranslatableAdmin


class AttributeAdmin(TranslatableAdmin):
    list_display = ('title','type')
