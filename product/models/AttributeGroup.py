from django.db import models
from parler.models import TranslatableModel, TranslatedFields


class AttributeGroup(TranslatableModel):

    translations = TranslatedFields(
        title=models.CharField('title', max_length=250),
    )

    def __str__(self):
        return self.safe_translation_getter('title', any_language=True)

    class Meta:
        verbose_name = "Attribute Group"
        verbose_name_plural = 'Attribute Groups'
