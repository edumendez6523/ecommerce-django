from django.db import models
from django.utils.safestring import mark_safe
from django.dispatch import receiver
from .Product import Product
import os


class Images(models.Model):
    id = models.AutoField(db_column="id", primary_key=True)
    image = models.ImageField(upload_to='product/images')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'

    def __str__(self):
        return 'image'

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.image.url)
        else:
            return 'No Image Found'

    image_tag.short_description = 'Product image'


@receiver(models.signals.post_delete, sender=Images)
def auto_delete_image_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


# images
@receiver(models.signals.pre_save, sender=Images)
def auto_delete_image_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        _Image = Images.objects.get(pk=instance.pk)
        if _Image.image:
            old_file = _Image.image
        else:
            old_file = None

    except Product.DoesNotExist:
        return False

    new_file = instance.image
    if old_file:
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)