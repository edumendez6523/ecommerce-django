from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from helper.helper import makeslug
from parler.models import TranslatableModel, TranslatedFields
from django.utils.safestring import mark_safe
from django.db.models.signals import pre_save, post_delete
from django.dispatch import receiver
import os


class Brand(TranslatableModel):
    image = models.ImageField(upload_to='images/brand', blank=True, null=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    translations = TranslatedFields(
        title=models.CharField('title', max_length=250, unique=True),
        slug=models.CharField('slug', max_length=255, unique=True)
    )

    def __str__(self):
        return self.safe_translation_getter('title', any_language=True)

    def create(self, *args, **kwargs):
        self.slug = makeslug(self.title)
        return super(Brand, self).create(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.slug = makeslug(self.slug)
        return super(Brand, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/brand" + "/%s/" % self.slug

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.image.url)
        else:
            return 'No Image Found'

    image_tag.short_description = 'Image'

    class Meta:
        verbose_name = "Brand"
        verbose_name_plural = 'Brands'
        ordering = ['order']


@receiver(post_delete, sender=Brand)
def auto_delete_image_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(pre_save, sender=Brand)
def auto_delete_image_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        user = Brand.objects.get(pk=instance.pk)
        if user.image:
            old_file = user.image
        else:
            old_file = None

    except Brand.DoesNotExist:
        return False

    new_file = instance.image
    if old_file:
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
