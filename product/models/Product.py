from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from helper.helper import makeslug
from parler.models import TranslatableModel, TranslatedFields
from django.utils.safestring import mark_safe
from django.db.models.signals import pre_save, post_delete
from django.dispatch import receiver
from ckeditor_uploader.fields import RichTextUploadingField
from .Category import Category
from .Brand import Brand
from mptt.models import TreeManyToManyField
from .Attribute import Attribute
import os


class Product(TranslatableModel):
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    price = models.DecimalField(max_digits=11, decimal_places=2)
    stock = models.IntegerField()
    categories = TreeManyToManyField(Category)
    attributes = models.ManyToManyField(Attribute,blank=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)

    translations = TranslatedFields(
        title=models.CharField('title', max_length=250, unique=True),
        slug=models.CharField('slug', max_length=255, unique=True),
        summary=RichTextUploadingField(config_name='ckeditor'),
        meta_description=models.TextField(verbose_name='Meta description', blank=True, null=True, default=""),
        meta_keywords=models.TextField(verbose_name='Meta keywords', blank=True, null=True, default="")
    )

    def __str__(self):
        return self.safe_translation_getter('title', any_language=True)

    def create(self, *args, **kwargs):
        self.slug = makeslug(self.title)
        return super(Product, self).create(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.slug = makeslug(self.slug)
        return super(Product, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/%s/" % self.slug

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = 'Products'
        ordering = ['order']
