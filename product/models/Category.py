from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from helper.helper import makeslug
from parler.models import TranslatableModel, TranslatedFields
from django.utils.safestring import mark_safe
from django.db.models.signals import pre_save, post_delete
from django.dispatch import receiver
from mptt.managers import TreeManager
from mptt.querysets import TreeQuerySet
from parler.managers import TranslatableQuerySet, TranslatableManager
from .AttributeGroup import AttributeGroup
import os


class CategoryQuerySet(TranslatableQuerySet, TreeQuerySet):

    def as_manager(cls):
        # make sure creating managers from querysets works.
        manager = CategoryManager.from_queryset(cls)()
        manager._built_with_as_manager = True
        return manager

    as_manager.queryset_only = True
    as_manager = classmethod(as_manager)


class CategoryManager(TreeManager, TranslatableManager):
    _queryset_class = CategoryQuerySet


class Category(MPTTModel, TranslatableModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    image = models.ImageField(upload_to='images/category', blank=True, null=True)
    attribute_group = models.ManyToManyField(AttributeGroup,blank=True)

    translations = TranslatedFields(
        title=models.CharField('title', max_length=250, unique=True),
        slug=models.CharField('slug', max_length=255, unique=True)
    )

    objects = CategoryManager()

    def __str__(self):
        return self.safe_translation_getter('title', any_language=True)

    def create(self, *args, **kwargs):
        self.slug = makeslug(self.title)
        return super(Category, self).create(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.slug = makeslug(self.slug)
        return super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        url = "/%s/" % self.slug
        page = self
        while page.parent:
            url = "/%s%s" % (page.parent.slug, url)
            page = page.parent
        return "/categories" + url

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.image.url)
        else:
            return 'No Image Found'
    image_tag.short_description = 'Image'

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = 'Categories'



@receiver(post_delete, sender=Category)
def auto_delete_image_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(pre_save, sender=Category)
def auto_delete_image_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        user = Category.objects.get(pk=instance.pk)
        if user.image:
            old_file = user.image
        else:
            old_file = None

    except Category.DoesNotExist:
        return False

    new_file = instance.image
    if old_file:
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
