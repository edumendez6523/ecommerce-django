from django.db import models
from .AttributeGroup import AttributeGroup
from parler.models import TranslatableModel, TranslatedFields


class Attribute(TranslatableModel):
    TYPE_CHOICES = [
        ('0', 'radio'),
        ('1', 'checkbox'),
    ]
    group = models.ForeignKey(AttributeGroup,on_delete=models.CASCADE)
    type = models.CharField(max_length=2,choices=TYPE_CHOICES,default='0')
    translations = TranslatedFields(
        title=models.CharField('title', max_length=250),
    )

    def __str__(self):
        return self.safe_translation_getter('title', any_language=True)

    class Meta:
        verbose_name = "Attribute"
        verbose_name_plural = 'Attributes'
        default_related_name = 'attrs'

