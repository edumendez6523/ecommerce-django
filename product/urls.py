from django.urls import path, re_path
from .views import show_categories, show_single, show_brands, get_categories, get_attributes

urlpatterns = [
    path('categories/', show_categories, name="categories"),
    path('brands/', show_brands, name="brands"),
    path('get-categories/', get_categories, name="get_categories"),
    path('get-attributes/', get_attributes, name="get_attributes"),
    re_path(r"^(?P<full_slug>.*)/$", show_single, name="show_single"),
]